import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:numero_jmp/pages/services/phone_number.dart';
import 'package:url_launcher/url_launcher.dart';
// ignore_for_file: prefer_const_constructors
class Private extends StatefulWidget {
  const Private({Key? key}) : super(key: key);

  @override
  _PrivateState createState() => _PrivateState();
}

class _PrivateState extends State<Private> {
  final _formKey = GlobalKey<FormState>();
  String _url = '';
  String numero = "";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      body: Padding(
        padding: const EdgeInsets.fromLTRB(15, 8, 15, 8),
        child: Column(
          children: <Widget>[
            Form(
              key: _formKey,
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.stretch,
                children: <Widget>[
                  TextFormField(
                    keyboardType: TextInputType.number,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Enter a phone number',
                    ),
                    // The validator receives the text that the user has entered.
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Phone number cannot be empty';
                      }
                      numero = value;
                      return null;
                    },
                  ),
                  SizedBox(height: 10),
                  ElevatedButton.icon(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        String numeroFinal = '';
                        setState(() {
                          numero = numero;
                        });
                        PhoneNumber phoneNumber = PhoneNumber();
                        phoneNumber.openPhoneNumber(numero);
                      }
                    },
                    icon: Icon(Icons.check),
                    label: Text("Submit"),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Center(
                      child: Text(numero, style: TextStyle(fontSize: 18)),
                    ),
                  )
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
