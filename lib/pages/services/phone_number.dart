import 'package:flutter/material.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:url_launcher/url_launcher.dart';

class PhoneNumber {
  String numeroFinal = "";
  String _url = "";
  String numero = "";
  String numbersString = "";

  PhoneNumber();

  openPhoneNumber(numero) {
    if (numero.startsWith("+1")
        || numero.startsWith("1.")
        || numero.startsWith("1-")
        || numero.startsWith("1 ")) {
      numero = numero.replaceAll(RegExp(r"\D"), "");
      numeroFinal = "xmpp:+$numero@cheogram.com?message";

      _url = numeroFinal;
      _launchURL();
    } else {
      numero = numero.replaceAll(RegExp(r"\D"), "");
      numeroFinal = "xmpp:+1$numero@cheogram.com?message";
      _url = numeroFinal;
      _launchURL();
    }
  }

  openGroupPhoneNumbers(groupArrayInt) {
    // create a string of the numbers
    numbersString = "+1" + groupArrayInt.join(",+1");
    numeroFinal = "xmpp:$numbersString@cheogram.com?message";
    _url = numeroFinal;
    _launchURL();
  }

  void _launchURL() async {
    try {
      if (!await launch(_url)) throw 'Could not launch $_url';
    } on PlatformException {
      // show error snackbar
      showNoAppError();
      // copy number
      Clipboard.setData(ClipboardData(text: numero));
      showCopiedToClipboard();
    }
  }

  void showNoAppError () {
    // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("Could not open app. Copied in clipboard instead.")));
  }

  void showCopiedToClipboard () {
    // ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text("$numero copied to clipboard")));
  }
}
