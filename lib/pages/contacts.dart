import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:numero_jmp/pages/services/phone_number.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:contacts_service/contacts_service.dart';
import 'package:permission_handler/permission_handler.dart';

// ignore_for_file: prefer_const_constructors

class Contacts extends StatefulWidget {
  const Contacts({Key? key}) : super(key: key);

  @override
  _ContactsState createState() => _ContactsState();
}

class _ContactsState extends State<Contacts> {
  final _formKey = GlobalKey<FormState>();
  List<Contact> contacts = [];
  List<Contact> selectedContacts = [];
  List<String> selectedContactsString = [];
  late bool isSelected;

  bool enableFAB = false;
  String numeroFinal = '';
  int numeroInt = 0;
  List<String> groupArray = [];
  List<int> groupArrayInt = [];
  String numbersString = "";

  String numero = "";
  late bool contactsEnabled;
  late PermissionStatus _status;

  @override
  void initState() {
    super.initState();
    _askPermission();
  }

  Future<void> _askPermission() async {
    await Permission.contacts.request();
    if (await Permission.contacts.status.isGranted) {
      await getContacts();
      contactsEnabled = true;
    } else {
      contactsEnabled = false;
      ScaffoldMessenger.of(context).showSnackBar(
          SnackBar(content: Text("Error: Could not load contacts.")));
    }
  }

  getContacts() async {
    List<Contact> _contacts = await ContactsService.getContacts();
    setState(() {
      contacts = _contacts;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,

      // TODO: Implement search bar

      // If enableFAB is true, display the FAB button. (true when contacts selected)
      floatingActionButton: (enableFAB)
          ? FloatingActionButton(
              onPressed: () {
                // opens the phone number URI
                PhoneNumber phoneNumber = PhoneNumber();
                phoneNumber.openGroupPhoneNumbers(groupArrayInt);
              },
              child: Icon(Icons.check),
            )
          : SizedBox.shrink(),
      body: ListView.builder(
        // physics: BouncingScrollPhysics(),
        itemCount: contacts.length,
        itemBuilder: (context, index) {
          Contact contact = contacts[index];
          if (contactsEnabled) {
            return ListTile(
              // if tile selected, set background to an accent color
              tileColor: (selectedContacts.contains(contact))
                  ? Theme.of(context).colorScheme.primaryContainer
                  : Colors.transparent,
              textColor: (selectedContacts.contains(contact))
                  ? Theme.of(context).colorScheme.onPrimaryContainer
                  : Theme.of(context).colorScheme.onBackground,
              // If there is no contact image, display the initials
              leading: (contact.avatar != null && contact.avatar!.isNotEmpty)
                  ? CircleAvatar(backgroundImage: MemoryImage(contact.avatar!))
                  : CircleAvatar(child:
                      Text(
                        contact.initials(),
                        style: TextStyle(color: Theme.of(context).colorScheme.onSecondary)),
                      backgroundColor: Theme.of(context).colorScheme.secondary,
                    ),

              title: Text(contact.displayName ??= "No display name"),
              subtitle: Text(contact.phones!.isEmpty
                  ? 'No phone number'
                  : contact.phones!.elementAt(0).value.toString()),

              // If the contact is selected, display a delete icon to unselect it
              trailing: (selectedContacts.contains(contact))
                  ? IconButton(
                      onPressed: () {
                        setState(() {
                          selectedContacts.remove(contact);
                          groupArrayInt.remove(int.parse(contact.phones!
                              .elementAt(0)
                              .value
                              .toString()
                              .replaceAll(RegExp(r"\D"), "")));
                          // if there are no selected contacts left, remove FAB button
                          if (selectedContacts.isEmpty) {
                            enableFAB = false;
                          }
                        });
                      },
                      icon: Icon(Icons.delete),
                    )
                  : SizedBox.shrink(),

              // TODO: show all phone numbers of the contact to chose one
              // select multiple contacts
              onLongPress: () {
                // check if the phone number is empty
                try {
                  numero = contact.phones!.elementAt(0).value.toString();
                  selectedContactsString.add(numero);
                  selectedContacts.add(contact);
                  isSelected = true;
                  // replace all non number characters in the number
                  numeroInt = int.parse(contact.phones!
                      .elementAt(0)
                      .value
                      .toString()
                      .replaceAll(RegExp(r"\D"), ""));
                  // add number to the group and sort the array
                  groupArrayInt.add(numeroInt);
                  groupArrayInt.sort();
                  // If multiple numbers selected, enable the FAB button
                  setState(() {
                    enableFAB = true;
                    groupArrayInt = groupArrayInt;
                  });
                } on RangeError {
                  // if the user tries selecting a number that does not exist
                  ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                      content: Text(
                          "Selected number is empty (${contact.displayName})")));
                }
              },

              // open the XMPP URI
              onTap: () {
                try {
                  numero = contact.phones!.elementAt(0).value.toString();
                  PhoneNumber phoneNumber = PhoneNumber();
                  phoneNumber.openPhoneNumber(numero);
                } on RangeError {
                  // if the user tries opening a non existing number
                  ScaffoldMessenger.of(context).showSnackBar(
                      SnackBar(content: Text("No phone number to open!")));
                }
              },
            );
          } else {
            return SizedBox.shrink();
          }
        },
      ),
    );
  }
}
