import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:numero_jmp/pages/home.dart';
import 'package:numero_jmp/pages/help.dart';
import 'package:numero_jmp/pages/private.dart';
import 'package:numero_jmp/pages/settings.dart';
import 'package:numero_jmp/pages/services/material_you_theme.dart';
import 'package:dynamic_color/dynamic_color.dart';
import 'package:shared_preferences/shared_preferences.dart';

void main() {
  runApp(const Numero_JMP());
}

class Numero_JMP extends StatelessWidget {
  const Numero_JMP({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.edgeToEdge, overlays: SystemUiOverlay.values);
    return DynamicColorBuilder(
        builder: (ColorScheme? lightTheme, ColorScheme? darkTheme) {
      return MaterialApp(
        themeMode: ThemeMode.system,
        theme: MaterialYouTheme.lightTheme(lightTheme), // LIGHT THEME
        darkTheme: MaterialYouTheme.darkTheme(darkTheme), // DARK THEME
        home: const Home(),
        // initialRoute: "/",
        routes: {
          // "/": (context) => const Home(),
          "/private": (context) => const Private(),
          "/settings": (context) => const Settings(),
          "/help": (context) => const Info(),
        },
      );
    });
  }
}
