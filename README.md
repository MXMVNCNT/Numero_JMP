<a href="https://apt.izzysoft.de/fdroid/index/apk/com.adaoh.numero.numero_jmp">
    <img src="https://gitlab.com/IzzyOnDroid/repo/-/raw/master/assets/IzzyOnDroid.png" width="280px"/>
</a>

# Numero_JMP

Flutter app for managing JMP.Chat/Cheogram numbers. It helps create group chats and makes adding phone numbers easier. In the future I plan on adding a contacts integration so that you only select contacts from your phone!

To build for yourself, you need to have flutter + dart as well as Android development tools (adb, etc.)

Once you have everything setup, go in the project directory and get/update the dependencies by runnning `flutter pub get`

After that, you can either plug in your device and run `flutter run`, this will start a debug version.
Or you can build an APK by running `flutter build apk`, this will build a release APK file.

# Screenshots 

<img src="https://codeberg.org/MXMVNCNT/Numero_JMP/raw/branch/main/screenshots/CONTACTS.png" width="32%" />
<img src="https://codeberg.org/MXMVNCNT/Numero_JMP/raw/branch/main/screenshots/PRIVATE.png" width="32%" />
<img src="https://codeberg.org/MXMVNCNT/Numero_JMP/raw/branch/main/screenshots/GROUP.png" width="32%" />