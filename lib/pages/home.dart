import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:numero_jmp/pages/contacts.dart';
import 'package:numero_jmp/pages/private.dart';
import 'package:permission_handler/permission_handler.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:url_launcher/url_launcher.dart';
// ignore_for_file: prefer_const_constructors

class Home extends StatefulWidget {
  const Home({Key? key}) : super(key: key);

  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends State<Home> {
  @override
  void initState() {
    super.initState();
    // _askPermission();
  }

  int _currentIndex = 0;

  List<Widget> screens = [
    Contacts(),
    Private(),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("JMP Numbers"),
        actions: [
          IconButton(
              onPressed: (){
                Navigator.pushNamed(context, "/help");
              },
              icon: Icon(Icons.info_outline))
        ],
      ),
      body: screens.elementAt(_currentIndex),
      drawer: Drawer(
        child: ListView(
          children: [
            ListTile(
              title: Text("Settings"),
              onTap: () {
                Navigator.pushNamed(context, "/settings");
              },
            )
          ],
        ),
      ),
      bottomNavigationBar: NavigationBar(
        selectedIndex: _currentIndex,
        onDestinationSelected: (index) {
          setState(() {
            _currentIndex = index;
          });
        },
        destinations: const [
          NavigationDestination(
            label: "Contacts",
            icon: Icon(Icons.group),
          ),
          NavigationDestination(
            label: "Private",
            icon: Icon(Icons.person),
          ),
        ],
      ),
    );
  }
}
