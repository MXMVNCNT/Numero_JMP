import 'package:flutter/material.dart';
class Info extends StatefulWidget {
  const Info({Key? key}) : super(key: key);

  @override
  State<Info> createState() => _InfoState();
}

class _InfoState extends State<Info> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).colorScheme.background,
      appBar: AppBar(
        title: Text("Help"),
      ),
      body: Padding(
        padding: EdgeInsets.all(15.0),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Text(
              "Contacts page",
              // style: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold)),
                style: Theme.of(context).textTheme.headline4
            ),
            const SizedBox(height: 10),
            Text(
                "To open an existing contact, simply tap on its tile. To create a group chat, you will need to long press every contact, and then press the checkmark button at the bottom",
                style: Theme.of(context).textTheme.bodyText1
            ),
            const SizedBox(height: 20),
            Text(
                "Custom page",
                style: Theme.of(context).textTheme.headline4
            ),
            const SizedBox(height: 10),
            Text(
                "This page is used for adding new contacts to JMP.Chat in an easy way. Simply put it in the field and press Submit!",
                style: Theme.of(context).textTheme.bodyText1
            ),
          ],
        ),
      ),
    );
  }
}
