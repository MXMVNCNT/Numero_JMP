import 'package:flutter/material.dart';

class MaterialYouTheme {
  static ThemeData lightTheme(ColorScheme? light) {
    ColorScheme theme = light ?? ColorScheme.fromSeed(seedColor: Colors.purple);
    return ThemeData(
      useMaterial3: true,
      colorScheme: theme,
      textTheme: TextTheme(
        headline4: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold, color: theme.onBackground),
        bodyText1: TextStyle(fontSize: 16.0, color: theme.onBackground),
      )
    );
  }

  static ThemeData darkTheme(ColorScheme? dark) {
    ColorScheme theme = dark ?? ColorScheme.fromSeed(seedColor: Colors.purple);
    return ThemeData(
      useMaterial3: true,
      colorScheme: theme,
      textTheme: TextTheme(
        headline4: TextStyle(fontSize: 24.0, fontWeight: FontWeight.bold, color: theme.onBackground),
        bodyText1: TextStyle(fontSize: 16.0, color: theme.onBackground),
      )
    );
  }
}
