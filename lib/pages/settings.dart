import 'package:flutter_settings_ui/flutter_settings_ui.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:flutter/material.dart';

// TODO: Add a color picker for the foreground elements (default cheogram purple)

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  Widget build(BuildContext context) {

    List themes = ['dark', 'light', 'custom'];
    int themeChoice = 0;

    setTheme(themeChoice) async {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      setState(() {
        prefs.setString("theme", themes[themeChoice]);
      });
    }
    
    return Scaffold(
      appBar: AppBar(
        title: const Text("Settings"),
      ),
      body: SettingsList(
        backgroundColor: Theme.of(context).colorScheme.background,
        // lightBackgroundColor: Colors.transparent,
        physics: const BouncingScrollPhysics(),
        sections: [
          SettingsSection(
            title: "Color Customization",
            titleTextStyle: const TextStyle(
              // color: Colors.purple,
            ),
            tiles: [
              SettingsTile(
                title: "Theme mode ($themeChoice)",
                onPressed: (BuildContext context) {
                  themeChoice++;
                  if (themeChoice > 2) {
                    themeChoice = 0;
                  }
                  setState(() {
                    themeChoice = themeChoice;
                    setTheme(themeChoice);
                  });
                },
              ),
            ]
          ),
        ],
      ),
    );
  }
}

